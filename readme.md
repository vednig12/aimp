Clone this repository using `git clone  https://gitlab.com/vednig12/AIMP`<br>
(If you don't have git installed go to https://git-scm.com/downloads and Download)<br>
this will download project in folder AIMP<br>
Then,<br>
Install and Run Python from https://python.org<br>
Then run `python -m pip install django`<br>
`python -m pip install pandas`<br>
`python -m pip install numpy`<br>
`python -m pip install torch`<br>
`python -m pip install tqdm`<br>


Open Terminal in  smartpm which is inside AIMP folder<br>
Finally run `python -m django manage.py runserver`<br>
And go to http://127.0.0.1:8000 in your browser.<br>
